package com.srinidhi.bellybuddy.print;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import android.content.ComponentName;
import android.content.Intent;
import android.os.Build;

import org.json.JSONArray;
import org.json.JSONException;
// import org.json.JSONObject;

/**
 * This class echoes a string called from JavaScript.
 */
public class BBPrint extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("print")) {
            String printData = args.getString(0);
            this.printMethod(printData, callbackContext);
            return true;
        }
        return false;
    }

    private void printMethod(String printData, CallbackContext callbackContext) {
        if (printData != null && printData.length() > 0) {
            Intent intent = new Intent();
            intent.setComponent(new ComponentName("co.srinidhi.backgroundprint",
                    "co.srinidhi.backgroundprint.service.BackgroundPrintingService"));
            intent.putExtra("Data", printData);
            startService(intent);
            callbackContext.success("success");
        } else {
            callbackContext.error("no print data found");
        }
    }

    private void startService(Intent intent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            this.cordova.getActivity().startForegroundService(intent);
        } else {
            this.cordova.getActivity().startService(intent);
        }
        // this.cordova.getActivity().startService(intent);
    }

}
